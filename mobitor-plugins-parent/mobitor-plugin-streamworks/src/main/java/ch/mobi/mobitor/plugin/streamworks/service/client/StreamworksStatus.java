package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksStream;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StreamworksStatus {
    private String verarbeitung;
    private LocalDate planDate;
    private String status;
    private List<StreamworksStream> streamworksStreams;
    private LocalDateTime lastUpdate;

    public StreamworksStatus() {
    }

    public StreamworksStatus(String verarbeitung,
                             LocalDate planDate,
                             String status,
                             List<StreamworksStream> streamworksStreams,
                             LocalDateTime lastUpdate) {
        this.verarbeitung = verarbeitung;
        this.planDate = planDate;
        this.status = status;
        this.streamworksStreams = streamworksStreams;
        this.lastUpdate = lastUpdate;
    }

    public LocalDate getPlanDate() {
        return planDate;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamworksStatus that = (StreamworksStatus) o;
        return verarbeitung.equals(that.verarbeitung) &&
                planDate.equals(that.planDate) &&
                status.equals(that.status) &&
                Objects.equals(streamworksStreams, that.streamworksStreams) &&
                lastUpdate.equals(that.lastUpdate);
    }

    @Override
    public String toString() {
        String streamsString = "";
        if (streamworksStreams != null) {
            streamsString = streamworksStreams.stream().map(StreamworksStream::toString).collect(Collectors.joining(",", "{", "}\n"));
        }
        return "StreamworksStatus{" +
                "verarbeitung='" + verarbeitung + '\'' +
                ", planDate=" + planDate +
                ", status='" + status + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", streamworksStreams='" + streamsString + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int streamHashcode = 0;
        if (streamworksStreams != null) {
            streamHashcode = streamworksStreams.hashCode();
        }
        return Objects.hash(verarbeitung, status, streamHashcode);
    }

    public String getVerarbeitung() {
        return verarbeitung;
    }

    public void setVerarbeitung(String verarbeitung) {
        this.verarbeitung = verarbeitung;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<StreamworksStream> getStreamworksStreams() {
        return streamworksStreams;
    }

    public void setStreamworksStreams(List<StreamworksStream> streamworksStreams) {
        this.streamworksStreams = streamworksStreams;
    }

}
