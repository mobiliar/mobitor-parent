package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.service.client.domain.ProjectComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SonarQubeProjectInformationProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(SonarQubeProjectInformationProviderService.class);

    private final SonarQubeClient sonarQubeClient;
    private final CoverageMetricService coverageMetricService;

    @Autowired
    public SonarQubeProjectInformationProviderService(SonarQubeClient sonarQubeClient, CoverageMetricService coverageMetricService) {
        this.sonarQubeClient = sonarQubeClient;
        this.coverageMetricService = coverageMetricService;
    }

    public void updateProjectInformation(SonarInformation sonarInformation) {
        ProjectComponent projectComponent = sonarQubeClient.retrieveProjectInformation(sonarInformation.getProjectKey());
        if (projectComponent != null) {
            sonarInformation.setBlockers(projectComponent.getBlockers());
            sonarInformation.setCriticals(projectComponent.getCriticals());
            sonarInformation.setCoverage(projectComponent.getCoverage());
            sonarInformation.setItCoverage(projectComponent.getItCoverage());
            sonarInformation.setOverallCoverage(projectComponent.getOverallCoverage());
            sonarInformation.setLineCoverage(projectComponent.getLineCoverage());
            sonarInformation.setDuplicatedBlocks(projectComponent.getDuplicatedBlocks());
            sonarInformation.setOverallLineCoverage(projectComponent.getOverallLineCoverage());

            LOG.info("updated sonarQube metric information for key: " + sonarInformation.getProjectKey());

            coverageMetricService.submitCoverage(sonarInformation);

        } else {
            LOG.warn("could not update sonarQube metric information for key: " + sonarInformation.getProjectKey());
        }
    }
}
