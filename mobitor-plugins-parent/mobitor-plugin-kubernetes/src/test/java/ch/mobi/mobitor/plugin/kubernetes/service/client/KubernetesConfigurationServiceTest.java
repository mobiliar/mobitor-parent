package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class KubernetesConfigurationServiceTest {

    @Test
    public void initializeKubernetesConfigService() {
        // arrange
        KubernetesConfigurationService k8sService = new KubernetesConfigurationService(new DefaultResourceLoader());
        k8sService.initializeKubernetesServers();

        // act
        List<KubernetesServerConfig> kubernetesServerConfigs = k8sService.getKubernetesServerConfigs();

        // assert
        assertThat(kubernetesServerConfigs).isNotEmpty();
        assertThat(kubernetesServerConfigs).hasSize(EnvironmentNetwork.values().length);

        assertNoUrlIsNull(kubernetesServerConfigs);
        assertNoNetworkIsNull(kubernetesServerConfigs);
    }

    private void assertNoNetworkIsNull(List<KubernetesServerConfig> kubernetesServerConfigs) {
        kubernetesServerConfigs.forEach(kubernetesServerConfig -> assertThat(kubernetesServerConfig.getNetwork()).isNotNull());
    }

    private void assertNoUrlIsNull(List<KubernetesServerConfig> kubernetesServerConfigs) {
        kubernetesServerConfigs.forEach(kubernetesServerConfig -> assertThat(kubernetesServerConfig.getUrl()).isNotNull());
    }

    @Test
    public void testEnvironmentIsMappedOnlyOnce() {
        // arrange
        KubernetesConfigurationService k8sService = new KubernetesConfigurationService(new DefaultResourceLoader());
        k8sService.initializeKubernetesServers();

        // act
        List<KubernetesServerConfig> kubernetesServerConfigs = k8sService.getKubernetesServerConfigs();

        List<String> allEnvs = new ArrayList<>();
        for (KubernetesServerConfig kubernetesServerConfig : kubernetesServerConfigs) {
            Set<String> environments = kubernetesServerConfig.getEnvironments();
            allEnvs.addAll(environments);
        }

        Set<String> envSet = new HashSet<>(allEnvs);

        // assert
        assertThat(envSet.size()).isEqualTo(allEnvs.size());
    }

    @Test
    public void testGetServerConfig() {
        // arrange
        KubernetesConfigurationService k8sService = new KubernetesConfigurationService(new DefaultResourceLoader());
        k8sService.initializeKubernetesServers();

        // act
        EnvironmentNetwork[] networks = EnvironmentNetwork.values();
        for (EnvironmentNetwork network : networks) {
            KubernetesServerConfig k8sConfig = k8sService.getKubernetesServerConfigForNetwork(network);
            // assert
            assertThat(k8sConfig).isNotNull();
        }
    }

}
