package ch.mobi.mobitor.plugin.jira.service.client;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResultResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.JiraIssueCustomFields;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static ch.mobi.mobitor.plugin.jira.service.JiraPluginConstants.CACHE_NAME_CHANGELOG_ISSUES;

@Component
public class JiraClient {

    public static final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    private static final Logger LOG = LoggerFactory.getLogger(JiraClient.class);

    private final JiraConfiguration jiraConfiguration;

    @Autowired
    public JiraClient(JiraConfiguration jiraConfiguration) {
        this.jiraConfiguration = jiraConfiguration;
    }

    public FilterResponse retrieveFilter(String filterId) {
        String filterUrl = jiraConfiguration.getBaseUrl() + "/rest/api/2/filter/" + filterId;

        // disable host name verification
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

        Executor executor = Executor.newInstance(httpClient);
        HttpHost host = HttpHost.create(jiraConfiguration.getBaseUrl());
        executor.auth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword()).authPreemptive(host);
        try {
            String response = executor
                    .execute(Request.Get(filterUrl)
                            .addHeader("accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createFilterResponse(response);

        } catch (Exception e) {
            LOG.error("Could not retrieve filter properties from Jira:" + filterUrl, e);
        }

        return null;
    }

    private FilterResponse createFilterResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        FilterResponse filterResponse = mapper.readValue(response, FilterResponse.class);
        return filterResponse;
    }

    private FilterResultResponse createFilterResultResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        FilterResultResponse filterResultResponse = mapper.readValue(response, FilterResultResponse.class);
        return filterResultResponse;
    }

    public FilterResultResponse retrieveFilterResult(String searchUrl) {
        try {
            // disable host name verification
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

            HttpHost host = HttpHost.create(jiraConfiguration.getBaseUrl());
            Executor executor = Executor.newInstance(httpClient);
            executor.auth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword()).authPreemptive(host);

            String response = executor
                    .execute(Request.Get(searchUrl)
                            .addHeader("accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createFilterResultResponse(response);

        } catch (Exception e) {
            LOG.error("Could not retrieve filter result from Jira: " + searchUrl, e);
        }

        return null;
    }

    @Cacheable(cacheNames = CACHE_NAME_CHANGELOG_ISSUES)
    public IssueResponse retrieveIssue(String issueKey) {
        String fixedIssueUrl = jiraConfiguration.getBaseUrl() + "/rest/api/2/issue/" + issueKey;
        try {
            // disable host name verification
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

            HttpHost host = HttpHost.create(jiraConfiguration.getBaseUrl());
            Executor executor = Executor.newInstance(httpClient);
            executor.auth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword()).authPreemptive(host);

            String response = executor
                    .execute(Request.Get(fixedIssueUrl)
                            .addHeader("accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createIssueResponse(response);

        } catch (Exception e) {
            LOG.error("Could not retrieve issue from Jira: " + fixedIssueUrl);
            LOG.debug("Jira Issue not found:", e);
        }

        return null;
    }

    private IssueResponse createIssueResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        IssueResponse issueResponse = mapper.readValue(response, IssueResponse.class);
        return issueResponse;
    }

    @CacheEvict(cacheNames = CACHE_NAME_CHANGELOG_ISSUES, key = "#issueKey")
    public void updateCustomField(String issueKey, String customFieldNameDeploymentEnvironments, Object newValue) {
        LOG.info("will set new custom field: " + newValue);

        String issueUrl = jiraConfiguration.getBaseUrl() + "/rest/api/2/issue/" + issueKey + "?notifyUsers=false";
        String responseBody = null;
        try {
            // disable host name verification
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();

            HttpHost host = HttpHost.create(jiraConfiguration.getBaseUrl());
            Executor executor = Executor.newInstance(httpClient);
            executor.auth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword()).authPreemptive(host);

            String json = createCustomFieldsJson(customFieldNameDeploymentEnvironments, newValue);
            Response response = executor
                    .execute(Request.Put(issueUrl)
                        .addHeader("accept", "application/json")
                        .bodyString(json, ContentType.APPLICATION_JSON)
                        .connectTimeout(3000)
                        .socketTimeout(3000)
                    );

            HttpResponse httpResponse = response.returnResponse();
            StatusLine statusLine = httpResponse.getStatusLine();
            responseBody = EntityUtils.toString(httpResponse.getEntity());
            if (statusLine.getStatusCode() >= 300) {
                throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
            }

        } catch (Exception e) {
            LOG.error("Could not update custom field for: " + issueUrl);
            LOG.error("Response Content: " + responseBody);
            if (responseBody == null) {
                LOG.error("Stacktrace for custom field update:", e);
            }
        }
    }

    String createCustomFieldsJson(String customFieldName, Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JiraIssueCustomFields jiraIssueCustomFields = new JiraIssueCustomFields();
        Map<String, Object> customFieldsMap = new HashMap<>();
        customFieldsMap.put(customFieldName, value);

        jiraIssueCustomFields.setFields(customFieldsMap);

        String json = mapper.writeValueAsString(jiraIssueCustomFields);

        return json;
    }
}
