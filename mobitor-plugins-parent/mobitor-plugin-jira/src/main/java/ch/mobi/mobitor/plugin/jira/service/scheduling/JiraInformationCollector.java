package ch.mobi.mobitor.plugin.jira.service.scheduling;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.jira.JiraPlugin;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.jira.service.client.JiraClient;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResultResponse;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.jira.domain.JiraInformation.JIRA;

@Component
@ConditionalOnBean(JiraPlugin.class)
public class JiraInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(JiraInformationCollector.class);

    private final ScreensModel screensModel;
    private final JiraClient jiraClient;
    private final RuleService ruleService;

    @Autowired
    public JiraInformationCollector(JiraClient jiraClient,
                                    ScreensModel screensModel,
                                    RuleService ruleService) {
        this.jiraClient = jiraClient;
        this.screensModel = screensModel;
        this.ruleService = ruleService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.jiraPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectJiraFilterInformation() {
        this.screensModel.getAvailableScreens().forEach(this::populateJiraFilterInformation);
    }

    private void populateJiraFilterInformation(Screen screen) {
        List<JiraInformation> jiraInformationList = screen.getMatchingInformation(JIRA);
        jiraInformationList.forEach(this::updateIssueCount);

        ruleService.updateRuleEvaluation(screen, JIRA);
        screen.setRefreshDate(JIRA, new Date());
    }

    private void updateIssueCount(JiraInformation jiraInformation) {
        updateSearchUrlForFilterId(jiraInformation);

        if (StringUtils.isNotEmpty(jiraInformation.getSearchUrl())) {
            FilterResultResponse filterResultResponse = jiraClient.retrieveFilterResult(jiraInformation.getSearchUrl());

            if (filterResultResponse != null) {
                jiraInformation.setCriticalIssues(filterResultResponse.countCriticalIssues());
                jiraInformation.setMajorIssues(filterResultResponse.countMajorIssues());
                jiraInformation.setTotal(filterResultResponse.getTotal());
                jiraInformation.setMaxResults(filterResultResponse.getMaxResults());

                jiraInformation.markAsUpdated();
            }

        } else {
            LOG.warn("Could not determine searchUrl for filter: " + jiraInformation.getFilterId());
        }
    }

    private void updateSearchUrlForFilterId(JiraInformation jiraInformation) {
        String filterId = jiraInformation.getFilterId();
        FilterResponse filterResponse = jiraClient.retrieveFilter(filterId);

        if (filterResponse != null) {
            jiraInformation.setSearchUrl(filterResponse.getSearchUrl());
            jiraInformation.setViewUrl(filterResponse.getViewUrl());
            jiraInformation.setFilterName(filterResponse.getName());
        }
    }

}
