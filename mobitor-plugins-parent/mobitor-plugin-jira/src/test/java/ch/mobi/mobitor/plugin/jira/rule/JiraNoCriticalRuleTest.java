package ch.mobi.mobitor.plugin.jira.rule;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import static ch.mobi.mobitor.plugin.jira.domain.JiraInformation.JIRA;
import static org.assertj.core.api.Assertions.assertThat;

public class JiraNoCriticalRuleTest extends PipelineRuleTest<JiraNoCriticalRule> {

    @Override
    protected JiraNoCriticalRule createNewRule() {
        return new JiraNoCriticalRule();
    }

    @Test
    public void evaluateRuleAllGood() {
        // arrange
        Pipeline pipeline = createPipeline();
        JiraInformation jiraInformation = new JiraInformation();
        pipeline.addInformation(ENV, APP_NAME, jiraInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    public void evaluateRuleWithViolations() {
        // arrange
        Pipeline pipeline = createPipeline();
        JiraInformation jiraInformation = new JiraInformation();
        jiraInformation.setCriticalIssues(1);

        pipeline.addInformation(ENV, APP_NAME, jiraInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isTrue();
    }

    @Test
    public void validatesType() {
        assertThat(validatesType(null)).isFalse();
        assertThat(validatesType(JIRA)).isTrue();
    }

}
