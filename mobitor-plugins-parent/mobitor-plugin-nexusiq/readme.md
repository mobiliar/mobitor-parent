Note:
==

The plugin has a list of policies for which to download violations. If policies are changed in Nexus IQ, than the list must be updated.

How to
==

- Open Postman
- Request the following: `https://nexusiq/api/v2/policies`
- Look for critical violations, from type 'license' and 'security'.
- Add the new policies to the list in a config file on your classpath: `classpath:mobitor/plugin/nexusiq/config/nexus-iq-configuration.json`

**N.B.:** Not all policies must be downloaded, some may be unused. Call the following to check if some violations are 
available for a given policy: `https://nexusiq/api/v2/policyViolations?p=<policy-id>`. 
E.g. `https://nexusiq/api/v2/policyViolations?p=bc0b83e88c0246b5adc8eb4b2852879e`
