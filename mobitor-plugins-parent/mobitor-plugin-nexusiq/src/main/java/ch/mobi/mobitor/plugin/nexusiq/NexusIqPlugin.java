package ch.mobi.mobitor.plugin.nexusiq;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.nexusiq.config.NexusIqApplicationConfig;
import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.nexusIqApplications.enabled", havingValue = "true")
public class NexusIqPlugin implements MobitorPlugin<NexusIqApplicationConfig> {

	private final NexusIqPluginConfiguration nexusIqPluginConfiguration;

	@Autowired
	public NexusIqPlugin(NexusIqPluginConfiguration nexusIqPluginConfiguration) {
		this.nexusIqPluginConfiguration = nexusIqPluginConfiguration;
	}

	@Override
	public String getConfigPropertyName() {
		return "nexusIqApplications";
	}

	@Override
	public Class<NexusIqApplicationConfig> getConfigClass() {
		return NexusIqApplicationConfig.class;
	}

	@Override
	public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig,
			List<NexusIqApplicationConfig> configs) {
		for (NexusIqApplicationConfig nexusIqApplicationConfig : configs) {
			String serverName = nexusIqApplicationConfig.getServerName();
			String applicationName = nexusIqApplicationConfig.getApplicationName();
			String publicId = nexusIqApplicationConfig.getPublicId();
			String env = nexusIqApplicationConfig.getEnvironment();
			Stage stage = nexusIqApplicationConfig.getStage();

			NexusIqInformation information = new NexusIqInformation(publicId, stage);
			information.setNoIssueUrl(nexusIqPluginConfiguration.getApplicationPageUrl() + publicId);

			screen.addInformation(serverName, applicationName, env, information);
		}
	}

	@Override
	public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
		return new NexusIqLegendGenerator().getLegendList();
	}

}
