package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus.HealthStatusResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class Jes2HealthStatusInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes2HealthStatusInterpreter createInterpreterWithHttpResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new Jes2HealthStatusInterpreter(executor);
    }

    private Jes2HealthStatusInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes2HealthStatusInterpreter(executor);
    }

    @Test
    public void testTkNameIdExtractionSuccess() {
        // arrange
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status.json", 200);

        // act
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        // assert
        String tkNameId = restServiceResponse.getProperty("tkNameId");
        assertThat(tkNameId).isEqualTo("app1-comp1");
    }

    @Override
    public void testInterpretSuccess() {
        // arrange
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status.json", 200);
        // Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile(, 200);

        // act
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        // assert
        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isTrue();
    }

    @Test
    public void testInterpretFailureWhenTelemetrieIsDisabled() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status_mon_disabled.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isFalse();
        assertThat(restServiceResponse.getMessages()).hasSize(1);
    }

    @Test
    public void testInterpretEmptyHealthStatus() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status_invalid_empty.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");
        String tkNameId = restServiceResponse.getProperty("tkNameId");

        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isFalse();
        assertThat(restServiceResponse.getMessages()).hasSize(1);

        assertThat(tkNameId).isNull();
    }

    @Override
    public void testInterpretFailure() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithErrorResponse("");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit");

        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isFalse();
    }

    @Override
    public void testMatchPredicate() {
        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 1);
    }

    @Test
    public void testCreateSwaggerResponseWithNull() {
        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(null);

        assertThrows(IllegalArgumentException.class, () -> interpreter.createHealthStatusResponse(null));
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException());

        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse(null);

        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isFalse();
    }

    @Test
    public void testInvalidJson() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithErrorResponse("alive!");

        assertThrows(JsonProcessingException.class, () -> interpreter.createHealthStatusResponse("alive!"));
    }

}
