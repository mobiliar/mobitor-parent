package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;
import ch.mobi.mobitor.plugin.rest.domain.swagger.Server;

public class ApiSpecResponseDeserializeTest {

    @Test
    public void testBuildResponseFinishedDeserialize() throws IOException {
        // arrange
        ApiSpecResponse sr = createSwaggerResponse("swagger-json-vertrag-example.json");

        // assert
        assertNotNull(sr);
        assertThat(sr.getSwaggerVersion()).isEqualTo("2.0");
        assertThat(sr.getBasePath()).isEqualTo("/context1/rest");

        assertThat(sr.getPaths()).containsKey("/health/status");
        assertThat(sr.getPaths()).containsKey("/verwaltung/ping");

        assertThat(sr.getPaths().get("/verwaltung/ping").supportsGet()).isTrue();
    }

    @Test
    public void testBuildResponseFinishedDeserializeOpenApi3() throws IOException {
        // arrange
        ApiSpecResponse sr = createSwaggerResponse("openapi3-json-vertrag-example.json");

        // assert
        assertNotNull(sr);
        assertThat(sr.getSwaggerVersion()).isNull();
        assertThat(sr.getBasePath()).isNull();

        assertThat(sr.getOpenApiVersion()).isEqualTo("3.0.1");
        assertThat(sr.getServers()).extracting(Server::getUrl).contains("/context1/rest");

        assertThat(sr.getPaths()).containsKey("/health/status");
        assertThat(sr.getPaths()).containsKey("/verwaltung/ping");

        assertThat(sr.getPaths().get("/verwaltung/ping").supportsGet()).isTrue();
    }

    @Test
    public void testBuildResponseFinishedDeserializeOpenApi3Yaml() throws IOException {
        // arrange
        ApiSpecResponse sr = createSwaggerResponseYaml("openapi3-example.yaml");

        // assert
        assertNotNull(sr);
        assertThat(sr.getSwaggerVersion()).isNull();
        assertThat(sr.getBasePath()).isNull();

        assertThat(sr.getOpenApiVersion()).isEqualTo("3.0.1");

        assertThat(sr.getPaths()).containsKey("/jap/liveness");
        assertThat(sr.getPaths()).containsKey("/jap/readiness");

    }

    @Test
    public void testSwaggerResponseRabatt() throws IOException {
        // arrange
        ApiSpecResponse sr = createSwaggerResponse("swagger-json-rabatt-example.json");

        // assert
        assertNotNull(sr);
    }

    @Test
    public void testSwaggerResponseJapHealth() throws IOException {
        // arrange
        ApiSpecResponse sr = createSwaggerResponse("swagger.json-context1-example.json");

        // assert
        assertNotNull(sr);
    }

    private ApiSpecResponse createSwaggerResponse(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        File swaggerFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + filename);
        // act
        return mapper.readValue(swaggerFile, ApiSpecResponse.class);
    }

    private ApiSpecResponse createSwaggerResponseYaml(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        File swaggerFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + filename);
        // act
        return mapper.readValue(swaggerFile, ApiSpecResponse.class);
    }
}
