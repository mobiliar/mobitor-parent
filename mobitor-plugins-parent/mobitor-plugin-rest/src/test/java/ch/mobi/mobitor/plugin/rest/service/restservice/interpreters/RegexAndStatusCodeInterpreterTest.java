package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class RegexAndStatusCodeInterpreterTest {

    private RegexAndStatusCodeInterpreter createInterpreterSuccessResponse(String response) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(response, 200);
        return new RegexAndStatusCodeInterpreter(executor);
    }

    private RegexAndStatusCodeInterpreter createInterpreterWithErrorResponse() {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor("", 505);
        return new RegexAndStatusCodeInterpreter(executor);
    }

    @Test
    public void fetchResponseWithoutRegex() {
        // arrange
        RegexAndStatusCodeInterpreter interpreter = createInterpreterSuccessResponse("");

        // act
        RestServiceResponse response = interpreter.fetchResponse("http://additionalUrl", null);

        // assert
        assertThat(response).isNotNull();
    }

    @Test
    public void fetchErrorStatusResponseWithoutRegex() {
        // arrange
        RegexAndStatusCodeInterpreter interpreter = createInterpreterWithErrorResponse();

        // act
        RestServiceResponse response = interpreter.fetchResponse("http://additionalUrl", null);

        // assert
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(505);
        assertThat(response.isSuccess()).isFalse();
    }

    @Test
    public void fetchResponseWithMatchingRegex() {
        // arrange
        RegexAndStatusCodeInterpreter interpreter = createInterpreterSuccessResponse("junit");

        // act
        RestServiceResponse response = interpreter.fetchResponse("http://additionalUrl", ".*[un].*+");

        // assert
        assertThat(response).isNotNull();
        assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void fetchResponseWithNotMatchingRegex() {
        // arrange
        RegexAndStatusCodeInterpreter interpreter = createInterpreterSuccessResponse("errorerrorerror");

        // act
        RestServiceResponse response = interpreter.fetchResponse("http://additionalUrl", ".*[un].*+");

        // assert
        assertThat(response).isNotNull();
        assertThat(response.getMessages()).isNotEmpty();
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException("junit exception"));

        RegexAndStatusCodeInterpreter interpreter = new RegexAndStatusCodeInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://additionalUri/", null);

        assertThat(restServiceResponse).isNotNull();
        assertThat(restServiceResponse.isSuccess()).isFalse();
    }


    @Test
    public void fetchResponseWithMatchingMultilineRegex() {
        // arrange
        String html = "<html lang=\"en\">\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>B2E Portal</title>\n" +
                "  <base href=\"/vvn-vertrieb-b2e/\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "<link rel=\"stylesheet\" href=\"/vvn-vertrieb-b2e/styles.9e780e3d8b67761610e6.css\"></head>\n" +
                "<body>\n" +
                "  <vvn-vertrieb-b2e-root>\n" +
                "    <div class=\"blockUI\">\n" +
                "      <div class=\"blockUI-backdrop\"></div>\n" +
                "      <div class=\"loader-box\">\n" +
                "        <div class=\"loader-message message message-flavor-info\">\n" +
                "          <div class=\"message-left loading-spinner\">\n" +
                "            <div class=\"spinner\">\n" +
                "              <div class=\"dot1\"></div>\n" +
                "              <div class=\"dot2\"></div>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "          <div class=\"message-center loader-messages\">\n" +
                "            <p>Initializing...</p>\n" +
                "          </div>\n" +
                "        </div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </vvn-vertrieb-b2e-root>\n" +
                "<script type=\"text/javascript\" src=\"/vvn-vertrieb-b2e/runtime.6b7bc2c08aad8ff9a736.js\"></script><script type=\"text/javascript\" src=\"/vvn-vertrieb-b2e/polyfills.abcdf1446c9320064bf8.js\"></script><script type=\"text/javascript\" src=\"/vvn-vertrieb-b2e/main.9b5aaedd12b73ec16209.js\"></script></body>\n" +
                "</html>";

        RegexAndStatusCodeInterpreter interpreter = createInterpreterSuccessResponse(html);

        // act
        RestServiceResponse response = interpreter.fetchResponse("http://additionalUrl", ".*<p>Initializing...</p>.*");

        // assert
        assertThat(response).isNotNull();
        assertThat(response.getMessages()).isEmpty();
    }

}
