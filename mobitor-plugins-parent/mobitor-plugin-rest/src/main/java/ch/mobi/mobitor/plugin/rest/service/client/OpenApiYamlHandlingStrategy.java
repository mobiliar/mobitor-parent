package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.http.client.fluent.Content;
import org.apache.http.entity.ContentType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;

@Component
public class OpenApiYamlHandlingStrategy implements ApiSpecHandlingStrategy {

    public static final String SUPPORTED_MIME_TYPE = ContentType.APPLICATION_OCTET_STREAM.getMimeType();

    @Override
    public boolean canHandleContent(Content content) {
        return SUPPORTED_MIME_TYPE.equals(content.getType().getMimeType());
    }

    @Override
    public String getAcceptHeaderValue() {
        return SUPPORTED_MIME_TYPE;
    }

    @Override
    public ApiSpecResponse deserializeResponse(Content apiSpecContent) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Charset charset = apiSpecContent.getType().getCharset();
        if (charset == null) {
            charset = StandardCharsets.UTF_8;
        }
        return mapper.readValue(apiSpecContent.asString(charset), ApiSpecResponse.class);
    }

}
