package ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TelemetryResponse implements Serializable {

    @JsonProperty private ReporterStatisticsResponse reporterStatistics;
    @JsonProperty private String appKey;
    @JsonProperty private String host;
    @JsonProperty private String appVersion;
    @JsonProperty private String stackVersion;

    public TelemetryResponse() {
    }

    public ReporterStatisticsResponse getReporterStatistics() {
        return reporterStatistics;
    }

    public void setReporterStatistics(ReporterStatisticsResponse reporterStatistics) {
        this.reporterStatistics = reporterStatistics;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getStackVersion() {
        return stackVersion;
    }

    public void setStackVersion(String stackVersion) {
        this.stackVersion = stackVersion;
    }
}
