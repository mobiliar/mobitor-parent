package ch.mobi.mobitor.plugin.rest.controller;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;

@Controller
public class RestController {

    private final ScreensModel screensModel;

    @Autowired
    public RestController(ScreensModel screensModel) {
        this.screensModel = screensModel;
    }

    @RequestMapping("/rest")
    public String overview(@RequestParam(value = "screen") String screenConfigKey,
                           @RequestParam(value = "env") String env,
                           @RequestParam(value = "server") String server,
                           @RequestParam(value = "application") String application,
                           Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        List<RestCallInformation> restCallInformationList = screen.getMatchingInformation(REST, env, server, application);

        model.addAttribute("restCalls", restCallInformationList);

        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("environment", env);
        model.addAttribute("serverName", server);
        model.addAttribute("applicationName", application);
        model.addAttribute("screen", screen);

        return "rest";
    }

}
