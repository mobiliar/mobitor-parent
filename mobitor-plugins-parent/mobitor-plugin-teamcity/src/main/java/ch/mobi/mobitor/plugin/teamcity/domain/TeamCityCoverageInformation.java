package ch.mobi.mobitor.plugin.teamcity.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class TeamCityCoverageInformation implements ApplicationInformation {

    public static final String TEAM_CITY_COVERAGE = "team_city_coverage";

    private final String configId;
    private String id;
    private final String label;
    private Date updated;
    private String status;
    private String webUrl;
    private BigDecimal coverage;

    public TeamCityCoverageInformation(String configId, String label) {
        this.configId = configId;
        this.label = label;
    }

    public String getConfigId() {
        return configId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSuccessful() {
        return "SUCCESS".equals(getStatus());
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public BigDecimal getCoverage() {
        if(coverage == null) {
            return BigDecimal.ZERO;
        }

        return coverage.setScale(1, RoundingMode.HALF_UP);
    }

    public void setCoverage(BigDecimal coverage) {
        this.coverage = coverage;
    }

    @Override
    public String getType() {
        return TEAM_CITY_COVERAGE;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

}
