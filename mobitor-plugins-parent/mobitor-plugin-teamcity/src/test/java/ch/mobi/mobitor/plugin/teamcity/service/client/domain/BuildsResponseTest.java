package ch.mobi.mobitor.plugin.teamcity.service.client.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BuildsResponseTest {
    @Test
    public void shouldHaveBuild() {
        BuildsResponse sut = aBuildsResponse(1);

        boolean result = sut.hasBuild();

        assertThat(result).isTrue();
    }

    @Test
    public void shouldNotHaveBuildWhenNull() {
        BuildsResponse sut = aBuildsResponse(-1);

        boolean result = sut.hasBuild();

        assertThat(result).isFalse();
    }

    @Test
    public void shouldNotHaveBuildWhenEmpty() {
        BuildsResponse sut = aBuildsResponse(0);
        sut.setBuilds(List.of());

        boolean result = sut.hasBuild();

        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnFirstBuild() {
        BuildResponse testResponse = new BuildResponse();
        BuildsResponse sut = aBuildsResponse(3);
        sut.getBuilds().add(0, testResponse);

        BuildResponse result = sut.getFirstBuild();

        assertThat(result).isSameAs(testResponse);
    }

    @Test
    public void shouldReturnNullIfNoBuilds() {
        BuildsResponse sutNull = aBuildsResponse(-1);
        BuildsResponse sutEmpty = aBuildsResponse(0);

        BuildResponse resultNull = sutNull.getFirstBuild();
        BuildResponse resultEmpty = sutEmpty.getFirstBuild();

        assertThat(resultNull).isNull();
        assertThat(resultEmpty).isNull();
    }

    @Test
    public void successfulIfEmpty() {
        BuildsResponse sut = aBuildsResponse(0);

        boolean result = sut.isFirstBuildSuccessfully();

        assertThat(result).isTrue();
    }

    @Test
    public void successfulIfSuccessful() {
        BuildsResponse sut = aBuildsResponse(0);
        sut.getBuilds().add(aBuildResponse(true));

        boolean result = sut.isFirstBuildSuccessfully();

        assertThat(result).isTrue();
    }

    @Test
    public void unsuccessfulIfUnsuccessful() {
        BuildsResponse sut = aBuildsResponse(0);
        sut.getBuilds().add(aBuildResponse(false));

        boolean result = sut.isFirstBuildSuccessfully();

        assertThat(result).isFalse();
    }

    private BuildsResponse aBuildsResponse(int numElements) {
        BuildsResponse result = new BuildsResponse();
        if (numElements >= 0) {
            List<BuildResponse> responses = new ArrayList();
            for (int i = 0; i<numElements; i++) {
                responses.add(aBuildResponse(false));
            }
            result.setBuilds(responses);
        }
        return result;
    }

    private BuildResponse aBuildResponse(boolean successful) {
        BuildResponse aResponse = new BuildResponse();
        if (successful) {
            aResponse.setStatus("SUCCESS");
        } else {
            aResponse.setStatus("Foobar");
        }
        return aResponse;
    }
}
