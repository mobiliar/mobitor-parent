package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.domain.ElanPatchStatus;

@Service
public class SwdDeploymentInformationProviderService {

    private final SwdClient swdClient;

    @Autowired
    public SwdDeploymentInformationProviderService(SwdClient swdClient) {
        this.swdClient = swdClient;
    }

    public void updateSwdDeploymentInformation(List<SwdDeploymentInformation> information) {
        information.forEach(swdInfo -> {
            String env = swdInfo.getEnvironment();
            ElanPatchStatus elanPatchStatus = swdClient.retrieveSwdDeploymentInformation(env);
            swdInfo.setPatchDate(elanPatchStatus.getStartTime());
            swdInfo.setRevision(elanPatchStatus.getGitRevision());
            swdInfo.setState(elanPatchStatus.getState());
            swdInfo.setId(elanPatchStatus.getId());
        });
    }
}
