package ch.mobi.mobitor.plugin.swd.service.scheduling;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.swd.SwdPlugin;
import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.SwdDeploymentInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation.SWD_DEPLOYMENT;

@Component
@ConditionalOnBean(SwdPlugin.class)
public class SwdDeploymentInformationCollector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final SwdDeploymentInformationProviderService providerService;

    @Autowired
    public SwdDeploymentInformationCollector(SwdDeploymentInformationProviderService providerService, ScreensModel screensModel, RuleService ruleService) {
        this.providerService = providerService;
        this.screensModel = screensModel;
        this.ruleService = ruleService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.swdDeploymentInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectSwdDeploymentInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving SWD deployment information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateSwdDeploymentInformation);
        long stop = System.currentTimeMillis();
        logger.info("Reading SWD deployment information took: " + (stop - start) + "ms");
    }

    private void populateSwdDeploymentInformation(Screen screen) {
        List<SwdDeploymentInformation> swdDeploymentInformationList = screen.getMatchingInformation(SWD_DEPLOYMENT);
        if (!swdDeploymentInformationList.isEmpty()) {
            providerService.updateSwdDeploymentInformation(swdDeploymentInformationList);
            ruleService.updateRuleEvaluation(screen, SWD_DEPLOYMENT);
            screen.setRefreshDate(SWD_DEPLOYMENT, new Date());
        }
    }
}
