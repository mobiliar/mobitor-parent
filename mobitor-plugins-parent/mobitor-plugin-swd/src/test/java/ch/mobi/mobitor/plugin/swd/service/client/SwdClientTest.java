package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import ch.mobi.mobitor.plugin.swd.service.client.domain.ElanPatchStatus;

public class SwdClientTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private SwdClient client;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port();

        SwdConfigurationService configurationService = new SwdConfigurationService();
        configurationService.setBaseUrl(baseUrl);

        wiremock.stubFor(get(urlMatching("/SWDPortalV2/api/elanpatching/elanpatchstatus.*"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("swd-deployments.json")));

        client = new SwdClient(configurationService);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }


    @Test
    public void retrieveSwdDeploymentInformationShouldReturnSwdDeployments() {
        //given
        //when
        ElanPatchStatus eps = client.retrieveSwdDeploymentInformation("P");

        //then
        assertThat(eps.getGitRevision()).isNotNull();
        assertThat(eps.getId()).isNotNull();
        assertThat(eps.getStartTime()).isNotNull();
        assertThat(eps.getState()).isNotNull();
    }

}
