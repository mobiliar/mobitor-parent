# Mobitor

This is the base module of the Mobitor application (the Mobiliar Build Monitor).

This base module contains the core plugins, the plugin interfaces and the (empty) Spring Boot application.

To use or create your own build Mobitor please refer to the [website](https://mobiliar.bitbucket.io/mobitor/)
of the latest release.

The snapshot build creates a [snapshot-website](https://mobiliar.bitbucket.io/mobitor-snapshot/).

If you want **to get started the repository to clone** is [mobitor-webapp](https://bitbucket.org/mobiliar/mobitor-webapp/).


## Swagger-UI

The Mobitor contains a Swagger-UI by including Springfox. It is accessible via /mobitor/swagger-ui.html
The swagger.json resides below: /mobitor/rest/swagger.json


## Prometheus export

The application uses Spring Boot Actuator and the Prometheus exporter to publish some metrics.

For example the duration the collectors that retrieve information is exported or the cache statistics.

The prometheus metrics can be viewed at: `/mobitor/actuator/prometheus`

## Developer notes

To release a new version, first create a new branch and push your changes in it. Then, create a pull request in Bitbucket. When the pipeline is green, ask some reviewer to approve your work and merge it to master branch.

## Admin notes

### Publishing

To start the publish process, go to the [bitbucket pipeline](https://bitbucket.org/mobiliar/mobitor-base/addon/pipelines/home) and click _Run pipeline_. Select _master_ as branch and _Custom:release_ as pipeline.

**Configuration**

The Central Maven Plugin requires a username and token to publish the artifacts. This username and token are stored as pipeline variable in Bibucket. You obtain a username and token from [Central Sonatype Repository](https://central.sonatype.com/). To login, use the credentials stored in the secret store of DLP (see in _Sonatype_ folder). Open _View Account_ in the profile and click _Generate User Token_. The current username and token are also stored in the secrete store. Update them if necessary.

Set the username and token in the pipeline variables in Bitbucket. The variables are named `OSS_SONATYPE_USERNAME` and `OSS_SONATYPE_TOKEN`. These variables are used in the `settings-sonatype.xml` file. The file is used by the Central Maven Plugin to publish the artifacts.
