package ch.mobi.mobitor.plugin.test.domain;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

public class TestRuleEvaluation implements RuleEvaluation {

    /** environment ("Y", "W", "X") is used as key for both multi maps */
    private ListMultimap<String, ApplicationInformation> violationsMap = ArrayListMultimap.create();
    private ListMultimap<String, String> messageMap = ArrayListMultimap.create();

    @Override
    public void addViolation(String env, ApplicationInformation applicationInformation, RuleViolationSeverity severity, String message) {
        violationsMap.put(env, applicationInformation);
        messageMap.put(env, message);
    }

    @Override
    public boolean hasErrors() {
        return this.violationsMap.size() > 0;
    }

    @Override
    public boolean hasWarnings() {
        return false;
    }

}
