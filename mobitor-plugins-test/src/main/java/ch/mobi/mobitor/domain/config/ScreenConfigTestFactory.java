package ch.mobi.mobitor.domain.config;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;

import java.util.Arrays;
import java.util.List;

public class ScreenConfigTestFactory {

    public static ExtendableScreenConfig createMinimalScreenConfig() {
        ExtendableScreenConfig screenConfig = new ExtendableScreenConfig();

        screenConfig.setEnvironments(Arrays.asList("Y", "X", "W"));
        List<String> appServers = Arrays.asList("appServ1", "appServ2");
        screenConfig.setServerNames(appServers);

        return screenConfig;
    }
}
