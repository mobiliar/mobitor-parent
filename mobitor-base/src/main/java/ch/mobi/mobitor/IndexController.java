package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.domain.screen.ScreenLabelComparator;
import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.config.MobitorApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class IndexController {

    private final ScreensModel screensModel;
    private final MobitorApplicationConfiguration mobitorConfiguration;
    private final ScreenViewHelper screenViewHelper;

    @Autowired
    public IndexController(ScreensModel screensModel,
                           MobitorApplicationConfiguration mobitorConfiguration,
                           ScreenViewHelper screenViewHelper) {
        this.screensModel = screensModel;
        this.mobitorConfiguration = mobitorConfiguration;
        this.screenViewHelper = screenViewHelper;
    }

    @RequestMapping("/")
    public String index(Model model) {
        List<Screen> screens = screensModel.getAvailableScreens();

        List<Screen> sortedScreens = new ArrayList<>(screens);
        sortedScreens.sort(new ScreenLabelComparator());

        model.addAttribute("screens", sortedScreens);
        model.addAttribute("applicationVersion", mobitorConfiguration.getVersion());
        model.addAttribute("activeProfiles", mobitorConfiguration.getActiveProfiles());
        model.addAttribute("network", mobitorConfiguration.getNetwork());
        model.addAttribute("svh", screenViewHelper);

        return "index";
    }
}
