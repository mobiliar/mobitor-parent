package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.configgenerator.ScreenConfigGenerator;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ExtendableScreenConfigService {

    private static final Logger LOG = LoggerFactory.getLogger(ExtendableScreenConfigService.class);

    private final MobitorPluginsRegistry pluginsRegistry;
    private final List<ScreenConfigGenerator> screenConfigGenerators;

    private List<ExtendableScreenConfig> configs = new ArrayList<>();

    @Autowired
    public ExtendableScreenConfigService(MobitorPluginsRegistry pluginsRegistry,
                                         List<ScreenConfigGenerator> screenConfigGenerators) {
        this.pluginsRegistry = pluginsRegistry;
        this.screenConfigGenerators = screenConfigGenerators;
    }

    @PostConstruct
    public void initialize() {
        PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();
        String resourceName = "";
        try {
            Resource[] resources = pathResolver.getResources("classpath*:screen/screen-*.json");
            for (Resource resource : resources) {
                resourceName = resource.getFilename();
                ExtendableScreenConfig conf = readConfig(resource);
                configs.add(conf);
            }
        } catch (IOException e) {
            LOG.error("Could not load screen-*.json config file [" + resourceName + "]!", e);
        } catch (IllegalArgumentException e) {
            LOG.error("Could not load screen-*.json config file [" + resourceName + "]!", e);
            throw e;
        }

        List<ExtendableScreenConfig> generatedScreenConfigs = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(screenConfigGenerators)) {
            for (ScreenConfigGenerator generator : screenConfigGenerators) {
                ExtendableScreenConfig extendableScreenConfig = generator.generateExtendableScreenConfig(configs);
                generatedScreenConfigs.add(extendableScreenConfig);
            }
        }
        configs.addAll(generatedScreenConfigs);
    }

    private ExtendableScreenConfig readConfig(Resource resource) {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try (InputStream inputStream = resource.getInputStream()) {
            ExtendableScreenConfig screenConfig = mapper.readValue(inputStream, ExtendableScreenConfig.class);

            String configKey = getConfigKey(resource.getFilename());
            screenConfig.setConfigKey(configKey);

            LOG.info(MessageFormat.format("successfully read screenconfig [{0}] with name [{1}]", configKey, screenConfig.getLabel()));

            // retain order from config file:
            Set<String> orderedPluginKeySet = screenConfig.getPluginConfigs().keySet();
            for (String pluginKey : orderedPluginKeySet) {
                MobitorPlugin plugin = pluginsRegistry.getPlugin(pluginKey);
                if (plugin != null) {
                    JsonNode pluginConfigNode = screenConfig.getPluginConfigNode(pluginKey);
                    JsonParser jsonParser = mapper.treeAsTokens(pluginConfigNode);

                    Class pluginConfigClass = plugin.getConfigClass();
                    LOG.debug("Parsing screen config for plugin " + plugin.getConfigPropertyName() + " with config class: " + pluginConfigClass);
                    CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(List.class, pluginConfigClass);

                    List<MobitorPlugin> pluginConfigObjList = mapper.readValue(jsonParser, typeReference);

                    screenConfig.putPluginConfigList(pluginKey, pluginConfigObjList);
                } else {
                    LOG.warn("Plugin with key not loaded: " + pluginKey);
                }
            }

            return screenConfig;

        } catch (Exception e) {
            throw new IllegalArgumentException("config could not be read", e);
        }
    }

    public List<ExtendableScreenConfig> getConfigs() {
        return configs;
    }

    private String getConfigKey(String screenConfigFilename) {
        Pattern screenPattern = Pattern.compile(".*screen-([0-9a-z]+)\\.json");
        Matcher matcher = screenPattern.matcher(screenConfigFilename);
        //noinspection ResultOfMethodCallIgnored
        matcher.find();
        String screenKey = matcher.group(1);
        return screenKey;
    }

    public ExtendableScreenConfig getScreenConfig(String configKey) {
        for (ExtendableScreenConfig screenConfig : this.configs) {
            if (screenConfig.getConfigKey().equals(configKey)) {
                return screenConfig;
            }
        }

        return null;
    }


    public List<ExtendableScreenConfig> getAllScreenConfigs() {
        return configs;
    }
}
