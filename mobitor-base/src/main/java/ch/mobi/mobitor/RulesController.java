package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RulesController {

    private final ScreensModel screensModel;

    @Autowired
    public RulesController(ScreensModel screensModel) {
        this.screensModel = screensModel;
    }

    @RequestMapping("/rules")
    public String overview(@RequestParam(value = "screen") String screenConfigKey,
                           @RequestParam(value = "server") String server,
                           Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        List<Pipeline> pipelineList = screen.getPipelines().stream().filter(pipeline -> pipeline.getAppServerName().equals(server)).collect(Collectors.toList());
        if (pipelineList.size() == 1) {
            model.addAttribute("pipeline", pipelineList.get(0));
        }

        model.addAttribute("server", server);
        model.addAttribute("screen", screen);

        return "rules";
    }
}
